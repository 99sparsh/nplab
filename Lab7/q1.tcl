set ns [new Simulator]

$ns color 1 Red
$ns color 2 Blue
$ns color 3 Green
$ns color 4 Yellow

set nf [open out.nam w]
$ns namtrace-all $nf

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam &
	exit 0
}

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]

$ns duplex-link $n0 $n4 2Mb 10ms DropTail
$ns duplex-link $n1 $n4 2Mb 10ms DropTail
$ns duplex-link $n2 $n4 2Mb 10ms DropTail
$ns duplex-link $n3 $n4 2Mb 10ms DropTail

# $ns queue-limit $n0 $n4 100
# $ns queue-limit $n4 $n3 100
# $ns queue-limit $n1 $n4 100
# $ns queue-limit $n4 $n2 100

$ns duplex-link-op $n0 $n4 orient down
$ns duplex-link-op $n1 $n4 orient up
$ns duplex-link-op $n2 $n4 orient right
$ns duplex-link-op $n3 $n4 orient left

$ns duplex-link-op $n0 $n4 queuePos 0.5
$ns duplex-link-op $n4 $n3 queuePos 0.5
$ns duplex-link-op $n1 $n4 queuePos 0.5
$ns duplex-link-op $n4 $n2 queuePos 0.5

set udp [new Agent/UDP]
$ns attach-agent $n0 $udp
set null [new Agent/Null]
$ns attach-agent $n3 $null
$ns connect $udp $null
$udp set fid_ 1
set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type_ CBR

# set udp [new Agent/UDP]
# $ns attach-agent $n4 $udp
# set null [new Agent/Null]
# $ns attach-agent $n3 $null
# $ns connect $udp $null
# $udp set fid_ 2
# set cbr2 [new Application/Traffic/CBR]
# $cbr2 attach-agent $udp
# $cbr2 set type_ CBR


set udp [new Agent/UDP]
$ns attach-agent $n1 $udp
set null [new Agent/Null]
$ns attach-agent $n2 $null
$ns connect $udp $null
$udp set fid_ 3
set cbr3 [new Application/Traffic/CBR]
$cbr3 attach-agent $udp
$cbr3 set type_ CBR

# set udp [new Agent/UDP]
# $ns attach-agent $n4 $udp
# set null [new Agent/Null]
# $ns attach-agent $n2 $null
# $ns connect $udp $null
# $udp set fid_ 4
# set cbr4 [new Application/Traffic/CBR]
# $cbr4 attach-agent $udp
# $cbr4 set type_ CBR



$cbr set packet_size_ 1000
$cbr set rate_ 1mb
$cbr set random_ false
# $cbr2 set packet_size_ 1000
# $cbr2 set rate_ 1mb
# $cbr2 set random_ false
$cbr3 set packet_size_ 1000
$cbr3 set rate_ 1mb
$cbr3 set random_ false
# $cbr4 set packet_size_ 1000
# $cbr4 set rate_ 1mb
# $cbr4 set random_ false



$ns at 0.1 "$cbr start"
$ns at 0.1 "$cbr3 start"

$ns at 9.0 "$cbr stop"
$ns at 9.0 "$cbr3 stop"

$ns at 10.0 "finish"

puts "CBR packet size = [$cbr set packet_size_]"
puts "CBR interval = [$cbr set interval_]"
puts "CBR packet size = [$cbr3 set packet_size_]"
puts "CBR interval = [$cbr3 set interval_]"


$ns run