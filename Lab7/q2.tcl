set ns [new Simulator]

$ns color 1 Red
$ns color 2 Blue

set nf [open out.nam w]
$ns namtrace-all $nf

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam &
	exit 0
}

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]

$ns duplex-link $n0 $n1 1Mb 10ms DropTail
$ns duplex-link $n2 $n3 1Mb 10ms DropTail
$ns duplex-link $n0 $n2 1Mb 10ms DropTail
$ns duplex-link $n1 $n3 1Mb 10ms DropTail

# $ns queue-limit $n0 $n1 10
# $ns queue-limit $n2 $n3 10

$ns duplex-link-op $n0 $n1 orient up
$ns duplex-link-op $n0 $n2 orient left
$ns duplex-link-op $n2 $n3 orient up
$ns duplex-link-op $n1 $n3 orient left

$ns duplex-link-op $n0 $n1 queuePos 0.5
$ns duplex-link-op $n2 $n3 queuePos 0.5

set tcp [new Agent/TCP]
set sink [new Agent/TCPSink]
$ns attach-agent $n0 $tcp 
$ns attach-agent $n1 $sink
$ns connect $tcp $sink
$tcp set fid_ 1
set ftp [new Application/FTP]
$ftp attach-agent $tcp
$ftp set type_ FTP

set tcp [new Agent/TCP]
set sink [new Agent/TCPSink]
$ns attach-agent $n2 $tcp 
$ns attach-agent $n3 $sink
$ns connect $tcp $sink
$tcp set fid_ 2
set ftp2 [new Application/FTP]
$ftp2 attach-agent $tcp
$ftp2 set type_ FTP

$ns at 1.0 "$ftp start"
$ns at 1.2 "$ftp2 start"
$ns at 9.0 "$ftp stop"
$ns at 9.2 "$ftp2 stop"
#$ns rtmodel-at 3.0 down $n0 $n1
$ns at 9.5 "$ns detach-agent $n0 $tcp ;$ns detach-agent $n1 $sink"
$ns at 9.5 "$ns detach-agent $n2 $tcp ;$ns detach-agent $n3 $sink"

$ns at 5.0 "finish"

$ns run