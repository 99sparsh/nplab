#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void main()
{
    int s,r,recb,sntb,x;

    struct sockaddr_in server;
    char buff[500];
    s=socket(AF_INET,SOCK_STREAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error.");
        exit(0);
    }

    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");

    r=connect(s,(struct sockaddr*)&server,sizeof(server));

    if(r==-1)
    {
        printf("\nConnection error.");
        exit(0);
    }

    while(1)
    {
        printf("Enter file name \n");
        scanf("%s",buff);
        if(strcmp(buff,"stop")==0)
            exit(0);
        sntb=send(s,buff,sizeof(buff),0);
        if(strcmp(buff,"exit")==0)
            break;
        recb=recv(s,buff,sizeof(buff),0);
        printf("%s",buff);
        if(strcmp(buff,"File Not Found")==0)
        {
            break;
        }
    }
}
